package br.com.feirpa.minhaagenda;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by thiago on 25/03/17.
 */

public class FormularioHelper {

    private Contato contato;

    private EditText txtNome;
    private EditText txtEmail;
    private EditText txtSite;
    private EditText txtTelefone;
    private EditText txtEndereco;

    private ImageView imgContato;

    private Button btnFoto;

    public FormularioHelper(Formulario activity) {
        this.contato = new Contato();

        this.txtNome = (EditText) activity.findViewById(R.id.txtFormName);
        this.txtEmail = (EditText) activity.findViewById(R.id.txtFormEmail);
        this.txtSite = (EditText) activity.findViewById(R.id.txtFormSite);
        this.txtTelefone = (EditText) activity.findViewById(R.id.txtFormPhone);
        this.txtEndereco = (EditText) activity.findViewById(R.id.txtFormAddress);

        this.imgContato = (ImageView) activity.findViewById(R.id.imgForm);

        this.btnFoto = (Button) activity.findViewById(R.id.btnFormAddImg);
    }

    public Button getBtnFoto() {
        return btnFoto;
    }

    public Contato getContatoForm() {

        contato.setNome(txtNome.getText().toString());
        contato.setEmail(txtEmail.getText().toString());
        contato.setSite(txtSite.getText().toString());
        contato.setTelefone(txtTelefone.getText().toString());
        contato.setEndereco(txtEndereco.getText().toString());

        contato.setFoto((String) imgContato.getTag());

        return contato;
    }

    public void setInForm(Contato contato) {
        txtNome.setText(contato.getNome());
        txtEmail.setText(contato.getEmail());
        txtSite.setText(contato.getSite());
        txtTelefone.setText(contato.getTelefone());
        txtEndereco.setText(contato.getEndereco());

        //imgContato.setTag(contato.getFoto());
        setImage(contato.getFoto());

        this.contato = contato;
    }

    public void setImage(String local) {
        if (local != null) {
            Bitmap imgFoto = BitmapFactory.decodeFile(local);
            imgContato.setImageBitmap(imgFoto);
            imgContato.setTag(local);
        }
    }
}
