package br.com.feirpa.minhaagenda;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.telephony.SmsMessage;

/**
 * Created by thiago on 28/03/17.
 */

public class SmsReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle bundle = intent.getExtras();
        Object[] menssagens = (Object[]) bundle.get("pdus");
        byte[] menssagem = (byte[]) menssagens[0];

        SmsMessage msg = SmsMessage.createFromPdu(menssagem);
        String telefone = msg.getDisplayOriginatingAddress();

        ContatoDAO dao = new ContatoDAO(context);

        if (dao.isContato(telefone)) {
            MediaPlayer player = MediaPlayer.create(context, R.raw.faustao_errou);
            player.start();
        }
    }
}
