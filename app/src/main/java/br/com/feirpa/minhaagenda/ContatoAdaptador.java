package br.com.feirpa.minhaagenda;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

/**
 * Created by thiago on 26/03/17.
 */

public class ContatoAdaptador extends BaseAdapter {

    private final List<Contato> contatos;
    private final Activity activity;

    public ContatoAdaptador(Activity activity, List<Contato> contatos) {
        this.contatos = contatos;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return this.contatos.size();
    }

    @Override
    public Object getItem(int position) {
        return this.contatos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return this.contatos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View linha = convertView;
        Contato contato = contatos.get(position);
        Bitmap bitmap;

        if (linha == null) {
            linha = this.activity.getLayoutInflater().inflate(R.layout.celula_contato, parent, false);
        }

        TextView txtNome = (TextView) linha.findViewById(R.id.txtCelulaNome);
        TextView txtEmail = (TextView) linha.findViewById(R.id.txtCelulaEmail);
        TextView txtEndereco = (TextView) linha.findViewById(R.id.txtCelulaEndereco);
        TextView txtSite = (TextView) linha.findViewById(R.id.txtCelulaSite);
        TextView txtTelefone = (TextView) linha.findViewById(R.id.txtCelulaTelefone);

        ImageView imgFoto = (ImageView) linha.findViewById(R.id.imgCelula);

        if (position % 2 == 0) {
            linha.setBackgroundColor(this.activity.getResources().getColor(R.color.corPar));
        } else {
            linha.setBackgroundColor(this.activity.getResources().getColor(R.color.corImpar));
        }

        txtNome.setText(contato.getNome());
        txtTelefone.setText(contato.getTelefone());

        if (txtEmail != null) {
            txtEmail.setText(contato.getEmail());
            txtEndereco.setText(contato.getEndereco());
            txtSite.setText(contato.getSite());
        }

        if (contato.getFoto() != null) {
            bitmap = BitmapFactory.decodeFile(contato.getFoto());
        } else {
            bitmap = BitmapFactory.decodeResource(activity.getResources(), R.mipmap.ic_launcher);
        }

        bitmap = Bitmap.createScaledBitmap(bitmap, 110, 110, true);

        imgFoto.setImageBitmap(bitmap);

        return linha;
    }
}
